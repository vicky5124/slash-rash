-- Add up migration script here
CREATE TABLE reminder (
    id SERIAL PRIMARY KEY,
    guild_id BIGINT,
    channel_id BIGINT NOT NULL,
    message_id BIGINT,
    user_id bigint NOT NULL,
    content TEXT,
    date timestamptz NOT NULL
);
