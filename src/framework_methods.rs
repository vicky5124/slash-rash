use crate::error::{CommandResult, Error};
use crate::global_data::Data;
use crate::Context;
use poise::FrameworkError;

pub async fn on_error(fwrk_error: FrameworkError<'_, Data, Error>) {
    match fwrk_error {
        FrameworkError::Setup { error: e } => panic!("Failed to start bot: {:?}", e),
        FrameworkError::Command { error: e, ctx } => match e {
            Error::RequiredArgument(why) => {
                let _ = crate::say(ctx, why.to_string()).await;
            }
            Error::JoinError(why) => {
                let _ = crate::say(ctx, why.to_string()).await;
            }
            _ => error!("Error in command `{}`: {:?}", ctx.command().name, e),
        },
        FrameworkError::CommandCheckFailed { error: e, .. } => {
            if let Some(e) = e {
                error!("CMD Check error: {:?}", e)
            }
        }
        FrameworkError::ArgumentParse {
            error: e,
            input,
            ctx,
        } => {
            if let Some(input) = &input {
                let _ = crate::say(
                    ctx,
                    format!(
                        "There was an error parsing this argument: ```\n{}\n```\n> {}",
                        input, e
                    ),
                )
                .await;
            } else {
                let _ = crate::say(
                    ctx,
                    format!("There was an error parsing arguments:\n> {}", e),
                )
                .await;
            }

            error!("Argument parsisng error: `{:?}` -> {}", input, e);
        }
        _ => error!("Other error: {:?}", fwrk_error),
    }
}

pub async fn dynamic_prefix<U, E>(ctx: poise::PartialContext<'_, U, E>) -> Option<String> {
    if ctx.guild_id.unwrap_or_default().0 == 182892283111276544 {
        Some("=".to_string())
    } else {
        Some(",".to_string())
    }
}

pub async fn pre_command(ctx: Context<'_>) {
    let is_prefix = match ctx {
        poise::Context::Prefix(_) => true,
        poise::Context::Application(_) => false,
    };

    let command = ctx.command();

    let position = ctx
        .framework()
        .options()
        .commands
        .iter()
        .position(|i| i == command);

    info!(
        "The user `{}` is running the command `{}` on category `{}` via `{}` with ID `{}`",
        ctx.author().tag(),
        command.identifying_name,
        command.category.unwrap_or("No Category"),
        if is_prefix { "Prefix" } else { "Slash Comamnd" },
        position.map(|i| i + 1).unwrap_or(0),
    );
}

pub async fn command_check(ctx: Context<'_>) -> CommandResult<bool> {
    if ctx
        .data()
        .disabled_commands
        .read()
        .contains(&ctx.command().identifying_name)
    {
        crate::send(ctx, |m| {
            m.content("This command has been globally disabled.");
            m.ephemeral(true);
            m
        })
        .await?;

        return Ok(false);
    }

    Ok(true)
}
