use lavalink_rs::error::LavalinkError;
use serenity::prelude::SerenityError;
use std::{error::Error as RawStdError, fmt, io::Error as StdIOError};

pub type StdError = Box<dyn RawStdError + Send + Sync>;
pub type CommandResult<T = (), E = Error> = Result<T, E>;

#[derive(Debug)]
pub enum Error {
    RequiredArgument(String),
    JoinError(String),

    Serenity(SerenityError),
    StdIOError(StdIOError),
    Lavalink(LavalinkError),
    Sqlx(sqlx::Error),
}

impl RawStdError for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::RequiredArgument(why) => write!(f, "Required Argument Error => {}", why),
            Self::JoinError(why) => write!(f, "Error while joining voice channel => {}", why),
            Self::Serenity(err) => write!(f, "Serenity Error => {}", err),
            Self::StdIOError(err) => write!(f, "std::io Error => {}", err),
            Self::Lavalink(err) => write!(f, "Lavalink Error => {}", err),
            Self::Sqlx(err) => write!(f, "SQLx Error => {}", err),
        }
    }
}

impl From<SerenityError> for Error {
    fn from(err: SerenityError) -> Self {
        Self::Serenity(err)
    }
}

impl From<StdIOError> for Error {
    fn from(err: StdIOError) -> Self {
        Self::StdIOError(err)
    }
}

impl From<LavalinkError> for Error {
    fn from(err: LavalinkError) -> Self {
        Self::Lavalink(err)
    }
}

impl From<sqlx::Error> for Error {
    fn from(err: sqlx::Error) -> Self {
        Self::Sqlx(err)
    }
}
