use crate::error::CommandResult;
use crate::global_data::Data;
use crate::rillrate_base::register_command_disable;

use std::sync::atomic::Ordering;

use poise::Event;
use serenity::prelude::Context as SerenityContext;

pub async fn handle_event<U, E>(
    ctx: &SerenityContext,
    event: &Event<'_>,
    framework: &poise::Framework<U, E>,
    data: &Data,
) -> CommandResult {
    match event {
        Event::Ready { data_about_bot: r } => info!("{} is ready to ROCK!", r.user.name),
        Event::CacheReady { .. } => {
            info!("Cache is ready.");

            if !data.already_running.swap(true, Ordering::Relaxed) {
                crate::loops::start_loops(ctx.clone(), data.clone()).await;

                for (idx, command) in framework.options().commands.iter().enumerate() {
                    register_command_disable(
                        idx + 1,
                        command.identifying_name.clone(),
                        command.category.unwrap_or("No Category"),
                        data.disabled_commands.clone(),
                    )
                    .await
                }
            }
        }
        _ => (),
    }

    Ok(())
}
