use crate::Context;
use regex::Regex;
use std::lazy::Lazy;

use serenity::model::{oauth2::OAuth2Scope, Permissions};

pub fn string_to_seconds(raw_text: impl ToString) -> u64 {
    let re = Lazy::new(|| {
        Regex::new(r"((?P<years>\d+?)Y|y|years)?((?P<months>\d+?)M|months)?((?P<weeks>\d+?)W|w|weeks)?((?P<days>\d+?)D|d|days)?((?P<hours>\d+?)H|h|hr|hours)?((?P<minutes>\d+?)m|min|minutes)?((?P<seconds>\d+?)S|s|sec|seconds)?").unwrap()
    });

    let text = raw_text.to_string();

    let captures = if let Some(caps) = re.captures(&text) {
        caps
    } else {
        return 0;
    };

    let mut seconds = 0;
    for name in [
        "years", "months", "weeks", "days", "hours", "minutes", "seconds",
    ] {
        if let Some(time) = captures.name(name) {
            let time: u64 = time.as_str().parse().unwrap();

            seconds += match name {
                "years" => time * 31_557_600, // 365.25 days (.25 to take leap years into account)
                "months" => time * 2_592_000, // 30 days
                "weeks" => time * 604_800,    // 7 days
                "days" => time * 86_400,      // 24 hours
                "hours" => time * 3_600,      // 60 minuts
                "minutes" => time * 60,       // 60 seconds
                "seconds" => time,            // yes
                _ => 0,
            };
        } else {
            continue;
        }
    }

    seconds
}

pub async fn generate_invite(ctx: &Context<'_>) -> String {
    let mut permissions = Permissions::empty();
    permissions.set(Permissions::ADMINISTRATOR, true);

    let scopes = vec![OAuth2Scope::Bot, OAuth2Scope::ApplicationsCommands];

    ctx.discord()
        .cache
        .current_user()
        .invite_url_with_oauth2_scopes(ctx.discord(), permissions, &scopes)
        .await
        .unwrap()
}
