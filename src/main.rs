#![feature(once_cell)]
#![deny(deprecated)]
#![deny(
    //clippy::unwrap_used,
    clippy::non_ascii_literal,
    clippy::let_underscore_must_use
)]

#[macro_use]
extern crate tracing;

pub mod basic_functions;
pub mod command_checks;
pub mod error;
pub mod events;
pub mod framework_methods;
pub mod global_data;
pub mod rillrate_base;

pub mod commands;
pub mod loops;

use std::{env, time::Duration};

use serenity::builder::{CreateAllowedMentions, ParseValue};
use songbird::SerenityInit;

use error::CommandResult;
pub type Context<'a> = poise::Context<'a, global_data::Data, error::Error>;

#[macro_export]
macro_rules! register_commands {
    ( $fwk: expr, $cmd: expr ) => ($fwk.commands.push($cmd()));
    ( $fwk: expr, $mdl: path, $( $cmd: tt ),+ ) => {
        {
            use $mdl as module;
            $(
                $fwk.commands.push(module::$cmd());
            )+
        }
    };
}

pub async fn say(
    ctx: Context<'_>,
    text: impl Into<String>,
) -> Result<Option<poise::ReplyHandle<'_>>, serenity::Error> {
    send(ctx, |m| m.content(text.into())).await
}

pub async fn say_ephemeral(
    ctx: Context<'_>,
    text: impl Into<String>,
) -> Result<Option<poise::ReplyHandle<'_>>, serenity::Error> {
    send(ctx, |m| {
        m.content(text.into());
        m.ephemeral(true);
        m
    }).await
}


pub async fn send<'a>(
    ctx: Context<'_>,
    builder: impl for<'b> FnOnce(&'b mut poise::CreateReply<'a>) -> &'b mut poise::CreateReply<'a>,
) -> Result<Option<poise::ReplyHandle<'_>>, serenity::Error> {
    ctx.send(|m| {
        m.allowed_mentions(|am| am.empty_parse());

        if let poise::Context::Prefix(pctx) = ctx {
            m.reference_message(pctx.msg);
        }

        builder(m);

        m
    })
    .await
}

#[tokio::main]
async fn main() -> Result<(), error::StdError> {
    env::set_var(
        "RUST_LOG",
        "info,slash_rash=trace,poise=debug,serenity=debug,meio=warn,rate_core=warn,rill_engine=warn,sqlx=warn,parking_lot=trace",
    );
    tracing_subscriber::fmt::init();

    dotenv::dotenv().unwrap();
    rillrate::install("Slash Rash")?;

    let mut options = poise::FrameworkOptions {
        prefix_options: poise::PrefixFrameworkOptions {
            edit_tracker: Some(poise::EditTracker::for_timespan(Duration::from_secs(500))),
            ignore_edit_tracker_cache: true,
            dynamic_prefix: Some(|ctx| Box::pin(framework_methods::dynamic_prefix(ctx))),
            ..Default::default()
        },
        on_error: |error| Box::pin(framework_methods::on_error(error)),
        pre_command: |ctx| Box::pin(framework_methods::pre_command(ctx)),
        listener: |ctx, event, framework, data| {
            Box::pin(events::handle_event(ctx, event, framework, data))
        },
        command_check: Some(|ctx| Box::pin(framework_methods::command_check(ctx))),
        allowed_mentions: Some(
            CreateAllowedMentions::default()
                .parse(ParseValue::Users)
                .parse(ParseValue::Roles)
                .clone(),
        ),
        ..Default::default()
    };

    register_commands!(options, commands::owner, register, unregister, test);
    register_commands!(options, commands::help::help);
    register_commands!(options, commands::meta, ping, about, run_yourself);
    register_commands!(options, commands::utils, remind_me);
    register_commands!(options, commands::info::user_info);
    register_commands!(options, commands::random::boop);
    register_commands!(options, commands::math, calculator);
    register_commands!(options, commands::music::basic, join, leave, play);

    let framework = poise::Framework::build()
        //.token(&env::var("SLASH_RASH")?)
        .token(&env::var("DISCORD_TOKEN")?)
        .options(options)
        .client_settings(|c| c.register_songbird())
        .user_data_setup(move |_ctx, ready, _framework| {
            Box::pin(async move { global_data::Data::new(ready).await })
        });

    framework.run().await?;

    Ok(())
}
