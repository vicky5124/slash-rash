use crate::{CommandResult, Context};

pub async fn is_owner(ctx: Context<'_>) -> CommandResult<bool> {
    Ok(ctx.author().id == ctx.data().owner_id)
}

pub async fn is_guild(ctx: Context<'_>) -> CommandResult<bool> {
    Ok(ctx.guild_id().is_some())
}
