use rillrate::prime::*;

pub const PACKAGE: &str = "Bot Dashboards";
pub const DASHBOARD_CONFIG: &str = "Config Dashboard";
pub const GROUP_CONF: &str = "1 - Enable or Disable commands";

pub async fn register_command_disable(
    id: usize,
    identifying_name: String,
    category: &'static str,
    disabled_commands: crate::global_data::DisabledCommands,
) {
    let switch = Switch::new(
        [
            PACKAGE,
            DASHBOARD_CONFIG,
            GROUP_CONF,
            &format!("{}: {} - Category {}", id, identifying_name, category,),
        ],
        SwitchOpts::default().label(format!(
            "{}: {} - Category: {}",
            id, identifying_name, category,
        )),
    );

    switch.apply(true);
    let switch_instance = switch.clone();

    tokio::spawn(async move {
        switch.sync_callback(move |envelope| {
            if let Some(action) = envelope.action {
                debug!(
                    "Switch action: {:?} | Comamnd {} on Category {} ID {}",
                    action, identifying_name, category, id
                );

                if action {
                    disabled_commands.write().remove(&identifying_name);
                } else {
                    disabled_commands
                        .write()
                        .insert(identifying_name.to_string());
                }

                switch_instance.apply(action);
            }

            Ok(())
        });
    });
}
