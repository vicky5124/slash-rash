use crate::global_data::Data;
use serenity::prelude::Context as SerenityContext;

pub mod reminders;

pub async fn start_loops(ctx: SerenityContext, data: Data) {
    info!("Starting all loops...");

    macro_rules! register_loop {
        ($p:path, $t:expr) => {
            let ctx_clone = ctx.clone();
            let data_clone = data.clone();

            tokio::spawn(async move {
                loop {
                    if let Err(why) = $p(&ctx_clone, &data_clone).await {
                        error!("Loop error on {} -> {:?}", stringify!($p), why);
                    }

                    tokio::time::sleep(std::time::Duration::from_secs($t)).await;
                }
            });
        };
    }

    register_loop!(reminders::send_reminders, 15);
}
