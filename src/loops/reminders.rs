use crate::global_data::Data;
use crate::CommandResult;
use serenity::model::id::ChannelId;
use serenity::prelude::Context as SerenityContext;

pub async fn send_reminders(ctx: &SerenityContext, data: &Data) -> CommandResult {
    let reminders = sqlx::query!("SELECT * FROM reminder WHERE date < NOW()")
        .fetch_all(&data.database)
        .await?;

    for reminder in reminders {
        info!("{:#?}", reminder);

        if let Err(why) = ChannelId(reminder.channel_id as u64)
            .send_message(ctx, |m| {
                m.allowed_mentions(|am| am.empty_parse());
                m.content(format!("<@!{}>", reminder.user_id));
                m.embed(|e| {
                    e.title("Reminder!");

                    if let Some(content) = reminder.content {
                        e.description(content);
                    }

                    if let Some(guild_id) = reminder.guild_id {
                        if let Some(message_id) = reminder.message_id {
                            e.url(format!(
                                "https://discord.com/channels/{}/{}/{}",
                                guild_id, reminder.channel_id, message_id
                            ));
                        }
                    }

                    e
                });
                m
            })
            .await
        {
            error!("Error sending reminder: {}", why);
        }

        sqlx::query!("DELETE FROM reminder WHERE id = $1", reminder.id)
            .execute(&data.database)
            .await?;
    }

    Ok(())
}
