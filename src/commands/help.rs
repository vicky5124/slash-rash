use crate::{CommandResult, Context};

/// Show this menu
#[poise::command(category = "Meta", prefix_command, slash_command, track_edits)]
pub async fn help(
    ctx: Context<'_>,
    #[description = "Specific command to show help about"] command: Option<String>,
) -> CommandResult {
    let bottom_text = "You can use `/help command` for more info on a specific command.\n\
                       You can edit your message to the bot and the bot will edit its response.";

    let help_config = poise::builtins::HelpConfiguration {
        extra_text_at_bottom: bottom_text,
        ephemeral: true,
        show_context_menu_commands: true,
    };

    poise::builtins::help(ctx, command.as_deref(), help_config).await?;

    Ok(())
}
