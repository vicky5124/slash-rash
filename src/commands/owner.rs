use crate::command_checks::is_owner;
use crate::{CommandResult, Context};

/// Register slash commands in this guild or globally
///
/// Run with no arguments to register in guild, run with argument "global" to register globally.
#[poise::command(category = "Owner", prefix_command, check = "is_owner", hide_in_help)]
pub async fn register(ctx: Context<'_>, #[flag] global: bool) -> CommandResult {
    let mut commands_builder = serenity::builder::CreateApplicationCommands::default();

    let commands = &ctx.framework().options().commands;

    for command in commands {
        if let Some(slash_command) = command.create_as_slash_command() {
            commands_builder.add_application_command(slash_command);
        }
        if let Some(context_menu_command) = command.create_as_context_menu_command() {
            commands_builder.add_application_command(context_menu_command);
        }
    }

    let commands_builder = serenity::json::Value::Array(commands_builder.0);

    crate::say(ctx, format!("Registering {} commands...", commands.len())).await?;

    if global {
        ctx.discord()
            .http
            .create_global_application_commands(&commands_builder)
            .await?;
    } else {
        ctx.discord()
            .http
            .create_guild_application_commands(ctx.guild_id().unwrap().0, &commands_builder)
            .await?;
    }

    crate::say(
        ctx,
        format!("Done registering {} commands!", commands.len()),
    )
    .await?;

    Ok(())
}

/// Unregister all slash commands.
#[poise::command(category = "Owner", prefix_command, check = "is_owner", hide_in_help)]
pub async fn unregister(ctx: Context<'_>, #[flag] global: bool) -> CommandResult {
    let commands_builder = serenity::builder::CreateApplicationCommands::default();
    let commands_builder = serenity::json::Value::Array(commands_builder.0);

    if global {
        ctx.discord()
            .http
            .create_global_application_commands(&commands_builder)
            .await?;
    } else {
        ctx.discord()
            .http
            .create_guild_application_commands(ctx.guild_id().unwrap().0, &commands_builder)
            .await?;
    }

    crate::say(ctx, format!("Unregistered...")).await?;

    Ok(())
}

/// Developer test command
#[poise::command(category = "Owner", prefix_command, check = "is_owner", hide_in_help, track_edits)]
pub async fn test(ctx: Context<'_>) -> CommandResult {
    crate::say(ctx, "test <@182891574139682816> <@&182894738100322304>").await?;
    crate::send(ctx, |m| {
        m.content("test <@182891574139682816> <@&182894738100322304>");
        m.embed(|e| e.title("test"));
        m
    })
    .await?;

    Ok(())
}
