use crate::error::Error;
use crate::{CommandResult, Context};

/// Calculates an expression.
///
/// Example: `/calculator 1+2*3/4^5%6 + log(100K) + log(e(),100) + [3*(3-3)/3] + (2<3) && 1.23`
///
/// The numbers are 64 bit (double precision) floating point values, here's some limitations:
/// - Floating point values behave a lot like the scientific notation (n*10^m)
/// - Sign bit: 1 bit
/// - Exponent: 11 bits
/// - Significand precision: 53 bits (52 explicitly stored)
/// - The precise integer limit is the signed 53 bits (-9007199254740993 to 9007199254740992)
/// - The the imprecise integer limit is almost the signed 1024 bit integer.
///
/// Supported operators:
/// ```
/// +               Addition
/// -               Subtraction
/// *               Multiplication
/// /               Division
/// %               Modulo
/// ^ **            Exponentiation
/// && (and)        Logical AND with short-circuit
/// || (or)         Logical OR with short-circuit
/// == != < <= >= > Comparisons (all have equal precedence)
///
/// ---------------
///
/// Integers: 1, 2, 10, 100, 1001
///
/// Decimals: 1.0, 1.23456, 0.000001
///
/// Exponents: 1e3, 1E3, 1e-3, 1E-3, 1.2345e100
///
/// Suffix:
/// 1.23p       = 0.00000000000123
/// 1.23n       = 0.00000000123
/// 1.23µ 1.23u = 0.00000123
/// 1.23m       = 0.00123
/// 1.23K 1.23k = 1230
/// 1.23M       = 1230000
/// 1.23G       = 1230000000
/// 1.23T       = 1230000000000
///
/// ---------------
///
/// e()  -- Euler's number (2.718281828459045)
/// pi() -- π (3.141592653589793)
///
/// log(base=10, val)
/// ---
/// Logarithm with optional 'base' as first argument.
/// If not provided, 'base' defaults to '10'.
/// Example: "log(100) + log(e(), 100)"
///
/// int(val)
/// ceil(val)
/// floor(val)
/// round(modulus=1, val)
/// ---
/// Round with optional 'modulus' as first argument.
/// Example: "round(1.23456) == 1  &&  round(0.001, 1.23456) == 1.235"
///
/// sqrt(val)
/// abs(val)
/// sign(val)
///
/// min(val, ...) -- Example: "min(1, -2, 3, -4) == -4"
/// max(val, ...) -- Example: "max(1, -2, 3, -4) == 3"
///
/// sin(radians)     asin(val)
/// cos(radians)     acos(val)
/// tan(radians)     atan(val)
/// sinh(val)        asinh(val)
/// cosh(val)        acosh(val)
/// tanh(val)        atanh(val)
/// ```
#[poise::command(
    category = "Mathematics",
    prefix_command,
    slash_command,
    track_edits,
    aliases("calc", "calculate", "math", "mathematics")
)]
pub async fn calculator(
    ctx: Context<'_>,
    #[description = "Mathematical Opration"]
    #[rest]
    mut operation: String,
) -> CommandResult {
    if operation.is_empty() {
        return Err(Error::RequiredArgument("An operation is required.".to_string()).into());
    }

    operation = operation.replace("**", "^");
    operation = operation.replace("pi()", "pi");
    operation = operation.replace("pi", "pi()");
    operation = operation.replace('\u{3c0}', "pi()"); // pi symbol "π"
    operation = operation.replace("euler", "e()");

    let mut operation_without_markdown = operation.replace(r"\\", r"\\\\");
    // " my ide is bugged lol

    for i in &["*", "`", "_", "~", "|"] {
        operation_without_markdown = operation_without_markdown.replace(i, &format!(r"\{}", i));
    }

    let mut cb = |name: &str, args: Vec<f64>| -> Option<f64> {
        match name {
            "sqrt" => {
                let a = args.get(0);
                if let Some(x) = a {
                    let l = x.log10();
                    Some(10.0_f64.powf(l / 2.0))
                } else {
                    None
                }
            }
            _ => None,
        }
    };

    let val = fasteval::ez_eval(&operation, &mut cb);

    match val {
        Err(why) => {
            use fasteval::error::Error;

            let text = match &why {
                Error::SlabOverflow => "Too many Expressions/Values/Instructions were stored in the Slab.".to_string(),
                Error::EOF => "Reached an unexpected End Of Input during parsing.\nMake sure your operators are complete.".to_string(),
                Error::EofWhileParsing(x) => format!("Reached an unexpected End Of Input during parsing:\n{}", x),
                Error::Utf8ErrorWhileParsing(_) => "The operator could not be decoded with UTF-8".to_string(),
                Error::TooLong => "The expression is too long.".to_string(),
                Error::TooDeep => "The expression is too recursive.".to_string(),
                Error::UnparsedTokensRemaining(x) => format!("An expression was parsed, but there is still input data remaining.\nUnparsed data: {}", x),
                Error::InvalidValue => "A value was expected, but invalid input data was found.".to_string(),
                Error::ParseF64(x) => format!("Could not parse a 64 bit floating point number:\n{}", x),
                Error::Expected(x) => format!("The expected input data was not found:\n{}", x),
                Error::WrongArgs(x) => format!("A function was called with the wrong arguments:\n{}", x),
                Error::Undefined(x) => format!("The expression tried to use an undefined variable or function, or it didn't provide any required arguments.:\n{}", x),
                Error::Unreachable => "This error should never happen, if it did, contact vicky5124#2207 immediately!".to_string(),
                _ => format!("An unhandled error occurred:\n{:#?}", &why),
            };

            crate::send(ctx, |m| {
                m.embed(|e| {
                    e.title("ERROR");
                    e.description(text);
                    e.field("Operation", &operation_without_markdown, true);
                    e.footer(|f| f.text(format!("Submitted by: {}", ctx.author().tag())))
                })
            })
            .await?;
        }

        Ok(res) => {
            crate::send(ctx, |m| {
                m.embed(|e| {
                    e.title("Result");
                    e.description(res);
                    e.field("Operation", &operation_without_markdown, true);
                    e.footer(|f| f.text(format!("Submitted by: {}", ctx.author().tag())))
                })
            })
            .await?;
        }
    }

    Ok(())
}
