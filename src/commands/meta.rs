use crate::{CommandResult, Context};
use serenity::client::bridge::gateway::ShardId;
use serenity::model::prelude::Message;

/// Runs the command in a message as if you did.
#[poise::command(category = "Meta", context_menu_command = "Run for yourself")]
pub async fn run_yourself(
    ctx: Context<'_>,
    #[description = "Command message to run (enter a link or ID)"] mut msg: Message,
) -> CommandResult {
    msg.author = ctx.author().clone();
    msg.id.0 = ctx.id();
    msg.guild_id = ctx.guild_id();

    if let Err(e) =
        poise::dispatch_message(ctx.framework(), ctx.discord(), &msg, false, false).await
    {
        match e {
            Some((err, _ctx)) => {
                crate::say_ephemeral(ctx, "There was an error running the command.").await?;

                crate::framework_methods::on_error(err).await;

                return Ok(());
            }

            None => {
                crate::say_ephemeral(ctx, "This message is not a bot command.").await?;
                return Ok(());
            }
        }
    }

    crate::say(ctx, &format!("Successfully ran command: `{}`", msg.content)).await?;

    Ok(())
}

/// Send the current shard latency.
#[poise::command(category = "Meta", prefix_command, slash_command, track_edits)]
pub async fn ping(ctx: Context<'_>) -> CommandResult {
    let ws_latency = {
        let shard_manager = ctx.framework().shard_manager();

        let manager = shard_manager.lock().await;
        let runners = manager.runners.lock().await;

        let runner = runners.get(&ShardId(ctx.discord().shard_id)).unwrap();

        if let Some(duration) = runner.latency {
            format!("{:.2}ms", duration.as_millis())
        } else {
            "?ms".to_string()
        }
    };

    crate::say(ctx, format!("The shard latency is {}", ws_latency)).await?;

    Ok(())
}

/// Send information about the bot.
#[poise::command(
    aliases("source", "invite"),
    category = "Meta",
    prefix_command,
    slash_command,
    track_edits
)]
pub async fn about(ctx: Context<'_>) -> CommandResult {
    let current_user = ctx.discord().cache.current_user();

    let invite_url = crate::basic_functions::generate_invite(&ctx).await;

    let bot_name = &current_user.name;
    let bot_icon = &current_user.face();

    let (hoster_team, hoster_tag, hoster_id) = {
        let app_info = ctx.discord().http.get_current_application_info().await?;

        if let Some(t) = app_info.team {
            (t.id.to_string(), t.members[0].user.tag(), t.owner_user_id)
        } else {
            ("None".to_string(), app_info.owner.tag(), app_info.owner.id)
        }
    };

    let command_count = ctx.framework().options().commands.len();

    let uptime = {
        let elapsed = chrono::offset::Utc::now().signed_duration_since(ctx.data().start_time);
        format!(
            "{:02}:{:02}:{:02} up {} days",
            elapsed.num_hours() % 24,
            elapsed.num_minutes() % 60,
            elapsed.num_seconds() % 60,
            elapsed.num_days()
        )
    };

    crate::send(ctx, |m| {
        m.embed(|e| {
            e.title(format!("**{}** - v{}", bot_name, env!("CARGO_PKG_VERSION")));
            e.url(env!("CARGO_PKG_REPOSITORY"));

            e.description(format!(
                "General Purpose Discord Bot made in [Rust](https://www.rust-lang.org/) using [poise](https://github.com/kangalioo/poise)\n\n\
                Having any issues? join the [Support Server](https://discord.gg/kH7z85n)\n\
                Be sure to [invite]({}) me to your server if you like what i can do!",
                invite_url,
            ));

            e.field("Some Stats:", format!("Uptime:\n`{}`\nCommand Count:\n`{}`", uptime, command_count), true);
            e.field("Currently owned by:", format!("Team: {}\nTag: {}\nID: {}", hoster_team, hoster_tag, hoster_id), true);

            e.thumbnail(bot_icon);

            e
        })
    }).await?;

    Ok(())
}
