use crate::{CommandResult, Context};
use serenity::model::prelude::User;

/// Query information about a Discord profile
#[poise::command(
    category = "Information",
    context_menu_command = "User information",
    slash_command,
    prefix_command,
    track_edits
)]
pub async fn user_info(
    ctx: Context<'_>,
    #[description = "Discord profile to query information about"] user: User,
) -> CommandResult {
    let response = format!(
        "**Name**: {}\n**Created**: {}\n{}",
        user.tag(),
        user.created_at(),
        user.face(),
    );

    crate::say(ctx, response).await?;

    Ok(())
}
