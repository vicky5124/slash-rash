use crate::{CommandResult, Context};
use serenity::collector::component_interaction_collector::CollectComponentInteraction;
use serenity::model::prelude::{message_component::ButtonStyle, InteractionResponseType};
use std::time::Duration;
use uuid::Uuid;

/// Boop the bot!
#[poise::command(category = "Random", prefix_command, slash_command)]
pub async fn boop(ctx: Context<'_>) -> CommandResult {
    let mut boop_count = 1;
    let uuid_boop = Uuid::new_v4().to_string();

    crate::send(ctx, |m| {
        m.content("I want some boops!".to_string());
        m.components(|c| {
            c.create_action_row(|ar| {
                ar.create_button(|b| {
                    b.style(ButtonStyle::Primary);
                    b.label("Boop me!");
                    b.custom_id(&uuid_boop);
                    b
                })
            })
        });
        m
    })
    .await?;

    loop {
        let mov_uuid_boop = uuid_boop.clone();
        let mci = CollectComponentInteraction::new(ctx.discord())
            .author_id(ctx.author().id)
            .channel_id(ctx.channel_id())
            .timeout(Duration::from_secs(120))
            .filter(move |mci| mci.data.custom_id == mov_uuid_boop)
            .await;

        if let Some(mci) = mci {
            let mut msg = mci.message.clone();
            msg.edit(ctx.discord(), |m| {
                m.content(format!("Boop count: {}", boop_count))
            })
            .await?;

            boop_count += 1;

            mci.create_interaction_response(ctx.discord(), |ir| {
                ir.kind(InteractionResponseType::DeferredUpdateMessage)
            })
            .await?;
        } else {
            debug!("Collector returned None, returning.");
            break;
        }
    }

    Ok(())
}
