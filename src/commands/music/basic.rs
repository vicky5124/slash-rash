use crate::command_checks::is_guild;
use crate::error::Error;
use crate::{CommandResult, Context};
use serenity::model::channel::GuildChannel;

/// Connects the bot to the voice channel you are currently on or argument.
#[poise::command(
    category = "Music",
    prefix_command,
    slash_command,
    track_edits,
    check = "is_guild",
    aliases("connect")
)]
pub async fn join(
    ctx: Context<'_>,
    #[description = "Voice channe to join"] voice_channel: Option<GuildChannel>,
) -> CommandResult {
    let channel_id = crate::commands::music::join_user_voice(&ctx, voice_channel).await?;
    crate::say(ctx, format!("Joined <#{}>", channel_id.0)).await?;

    Ok(())
}

/// Disconnects the bot from the voice channel if im in one.
#[poise::command(
    category = "Music",
    prefix_command,
    slash_command,
    track_edits,
    check = "is_guild",
    aliases("disconnect")
)]
pub async fn leave(ctx: Context<'_>) -> CommandResult {
    let guild = ctx.guild().unwrap();
    let guild_id = guild.id;

    let manager = songbird::get(ctx.discord()).await.unwrap().clone();
    let has_handler = manager.get(guild_id).is_some();

    if has_handler {
        if let Err(e) = manager.remove(guild_id).await {
            error!("Failed to disconnect from voice channel: {:?}", e);

            crate::say(ctx, format!("Failed: {:?}", e)).await?;
        }

        let lava_client = ctx.data().lavalink.clone();

        lava_client.destroy(guild_id).await?;
        lava_client.nodes().await.remove(&guild_id.0);

        let loops = lava_client.loops().await;

        loops.remove(&guild_id.0);

        crate::say(ctx, "Successfully left the voie channel.").await?;
    } else {
        crate::say(ctx, "Not in a voice channel").await?;
    }

    Ok(())
}

/// Adds a song to the queue.
///
/// Usage: `/play starmachine2000`
/// or `/play https://www.youtube.com/watch?v=dQw4w9WgXcQ`
#[poise::command(
    category = "Music",
    prefix_command,
    slash_command,
    track_edits,
    check = "is_guild",
    aliases("p")
)]
pub async fn play(
    ctx: Context<'_>,
    #[description = "Search Query or URL to play"]
    #[rest]
    mut query: String,
) -> CommandResult {
    let mut embeded = false;
    let guild_id = ctx.guild_id().unwrap();

    if query.is_empty() {
        return Err(Error::RequiredArgument("Missing the search query or URL".to_string()).into());
    }

    if songbird::get(ctx.discord())
        .await
        .unwrap()
        .get(guild_id)
        .is_none()
    {
        crate::commands::music::join_user_voice(&ctx, None).await?;
    }

    if query.starts_with('<') && query.ends_with('>') {
        embeded = true;
        let re = regex::Regex::new("[<>]").unwrap();
        query = re.replace_all(&query, "").into_owned();
    }

    let mut m = None;

    if let poise::Context::Prefix(pctx) = ctx {
        if pctx
            .discord
            .http
            .edit_message(
                pctx.msg.channel_id.0,
                pctx.msg.id.0,
                &serenity::json::json!({"flags" : 4}),
            )
            .await
            .is_err()
            && query.starts_with("http")
            && !embeded
        {
            m = Some(crate::say(ctx, "Please, put the url between <> so it doesn't embed.").await?);
        }
    }

    let manager = songbird::get(ctx.discord()).await.unwrap().clone();

    if let Some(_handler_lock) = manager.get(guild_id) {
        let lava_client = ctx.data().lavalink.clone();

        let mut iter = 0;
        let mut already_checked = false;

        let query_information = loop {
            iter += 1;
            let res = lava_client.auto_search_tracks(&query).await?;

            if res.tracks.is_empty() {
                if iter == 2 {
                    if !already_checked {
                        already_checked = true;

                        let output: std::process::Output = tokio::process::Command::new("yt-dlp")
                            .arg("-g")
                            .arg(&query)
                            .output()
                            .await?;

                        if !output.stdout.is_empty() {
                            let stdout = String::from_utf8(output.stdout).unwrap_or_default();

                            if !stdout.is_empty() {
                                let mut stdout = stdout.split('\n').collect::<Vec<_>>();
                                stdout.pop();
                                let url = stdout.last().unwrap();

                                iter = 0;
                                query = url.to_string();

                                continue;
                            }
                        }
                    }
                    crate::say(ctx, "Could not find any video of the search query.").await?;
                    return Ok(());
                }
            } else {
                if query.starts_with("http") && res.tracks.len() > 1 {
                    crate::send(ctx, |m| {
                        m.content("If you would like to play the entire playlist, use `play_playlist` instead.");
                        m.ephemeral(true);
                        m
                    }).await?;
                }
                break res;
            }
        };

        lava_client
            .play(guild_id, query_information.tracks[0].clone())
            .requester(ctx.author().id)
            .queue()
            .await?;

        let mut position = 1;

        if let Some(node) = lava_client.nodes().await.get_mut(&guild_id.0) {
            position = node.queue.len() - 1;
        };

        crate::send(ctx, |m| {
            m.content(format!("Added to queue at position {}", position));
            m.embed(|e| {
                e.title(&query_information.tracks[0].info.as_ref().unwrap().title);
                e.thumbnail(format!(
                    "https://i.ytimg.com/vi/{}/default.jpg",
                    query_information.tracks[0]
                        .info
                        .as_ref()
                        .unwrap()
                        .identifier
                ));
                e.url(&query_information.tracks[0].info.as_ref().unwrap().uri);
                e.footer(|f| f.text(format!("Submited by {}", ctx.author().tag())));
                e.field(
                    "Uploader",
                    &query_information.tracks[0].info.as_ref().unwrap().author,
                    true,
                );
                e.field(
                    "Length",
                    {
                        let length =
                            query_information.tracks[0].info.as_ref().unwrap().length / 1000;

                        let minutes = length % 3600 / 60;
                        let seconds = length % 3600 % 60;

                        if length >= 3600 {
                            let hours = length / 3600;
                            format!("{}:{:02}:{:02}", hours, minutes, seconds)
                        } else {
                            format!("{:02}:{:02}", minutes, seconds)
                        }
                    },
                    true,
                );
                e
            })
        })
        .await?;
    } else {
        crate::say(ctx, "Please, connect the bot to the voice channel you are currently on first with the `join` command.").await?;
    }

    if let Some(m) = m {
        tokio::time::sleep(tokio::time::Duration::from_secs(2)).await;
        let _ = m.unwrap().message().await?.delete(ctx.discord()).await;
    }

    Ok(())
}
