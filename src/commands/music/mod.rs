use crate::error::Error;
use crate::{CommandResult, Context};
use serenity::model::channel::{ChannelType, GuildChannel};
use serenity::model::id::ChannelId;

pub mod advanced;
pub mod basic;

pub async fn join_user_voice(
    ctx: &Context<'_>,
    channel: Option<GuildChannel>,
) -> CommandResult<ChannelId> {
    let guild = ctx.guild().unwrap();
    let guild_id = guild.id;

    let channel_id = match channel.as_ref().map(|c| c.kind) {
        Some(ChannelType::Voice) => Some(channel.unwrap().id),
        _ => guild
            .voice_states
            .get(&ctx.author().id)
            .and_then(|voice_state| voice_state.channel_id),
    };

    let connect_to = match channel_id {
        Some(channel) => channel,
        None => {
            return Err(
                Error::JoinError("You are not connected to a voice channel".to_string()).into(),
            );
        }
    };

    let manager = songbird::get(ctx.discord()).await.unwrap().clone();

    let (_, handler) = manager.join_gateway(guild_id, connect_to).await;

    match handler {
        Ok(connection_info) => {
            let lava_client = ctx.data().lavalink.clone();
            lava_client
                .create_session_with_songbird(&connection_info)
                .await?;

            Ok(connect_to)
        }
        Err(why) => {
            error!("Error joining voice channel: {}", why);
            Err(Error::JoinError("Error joining the channel".to_string()).into())
        }
    }
}
