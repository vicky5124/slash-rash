use crate::{CommandResult, Context};

/// Remind you of something after some time
#[poise::command(
    aliases("reminder", "remind"),
    category = "Utils",
    slash_command,
    prefix_command
)]
pub async fn remind_me(
    ctx: Context<'_>,
    #[description = "After how much time should I remind you?"] date: String,
    #[description = "What should I remind you?"]
    #[rest]
    text: Option<String>,
) -> CommandResult {
    let seconds = crate::basic_functions::string_to_seconds(date);

    if seconds < 60 {
        crate::say(ctx, "Minimum allowed time is a minute!").await?;
        return Ok(());
    }

    let date = chrono::offset::Utc::now() + chrono::Duration::seconds(seconds as i64);

    let message_id = {
        if let poise::Context::Prefix(pctx) = ctx {
            Some(pctx.msg.id.0 as i64)
        } else {
            None
        }
    };

    let query = sqlx::query!(
        "INSERT INTO reminder (guild_id, channel_id, message_id, user_id, content, date) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id",
        ctx.guild_id().map(|id| id.0 as i64), ctx.channel_id().0 as i64, message_id, ctx.author().id.0 as i64, text, date
    )
    .fetch_one(&ctx.data().database)
    .await?;

    crate::say(
        ctx,
        format!(
            "Reminder saved! You will be reminded of this <t:{}:F>!\nReminder ID: {}",
            date.timestamp(),
            query.id
        ),
    )
    .await?;

    Ok(())
}
