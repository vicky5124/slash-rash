#![allow(clippy::let_underscore_must_use)]

use std::sync::Arc;
use std::{collections::HashSet, sync::atomic::AtomicBool};

use crate::error::CommandResult;
use parking_lot::RwLock;
use serenity::model::prelude::{Ready, UserId};
use sqlx::postgres::{PgPool, PgPoolOptions};

pub type DisabledCommands = Arc<RwLock<HashSet<String>>>;

struct LavalinkHandler;
#[lavalink_rs::async_trait]
impl lavalink_rs::gateway::LavalinkEventHandler for LavalinkHandler {}

#[derive(derivative::Derivative)]
#[derivative(Debug, Clone)]
pub struct Data {
    pub owner_id: UserId,
    pub already_running: Arc<AtomicBool>,
    pub start_time: chrono::DateTime<chrono::offset::Utc>,
    #[derivative(Debug = "ignore")]
    pub lavalink: lavalink_rs::LavalinkClient,
    pub disabled_commands: DisabledCommands,
    pub database: PgPool,
}

impl Data {
    pub async fn new(ready: &Ready) -> CommandResult<Self> {
        let lavalink = lavalink_rs::LavalinkClient::builder(ready.user.id.0)
            .set_password(env!("LAVALINK_PASSWORD"))
            .build(LavalinkHandler)
            .await?;

        let database = PgPoolOptions::new()
            .max_connections(20)
            .connect(&std::env::var("DATABASE_URL").unwrap())
            .await?;

        Ok(Data {
            owner_id: UserId(182891574139682816),
            already_running: Arc::new(AtomicBool::new(false)),
            start_time: chrono::offset::Utc::now(),
            lavalink,
            disabled_commands: Arc::new(RwLock::new(HashSet::new())),
            database,
        })
    }
}
